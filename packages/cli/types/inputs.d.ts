import { supported } from '@/store'
// TYPES
import type { Separator } from '@inquirer/prompts'
import type { TupleToUnionType } from './utils'

export type Choice<ValueType> =
  | {
      value: ValueType
      name?: string
      description?: string
      disabled?: boolean | string
    }
  | Separator

// CLI
export type SupportedPackageManager = TupleToUnionType<
  typeof supported.packageManager
>
export type SupportedLanguage = TupleToUnionType<typeof supported.languages>
// PROVIDERS
export type SupportedRegistryProvider = TupleToUnionType<
  typeof supported.providers.registry
>
export type SupportedCloudProvider = TupleToUnionType<
  typeof supported.providers.cloud
>
// SERVICES
export type SupportedAnalyticsService = TupleToUnionType<
  typeof supported.services.analytics
>
export type SupportedMonitoringService = TupleToUnionType<
  typeof supported.services.monitoring
>
export type SupportedService =
  | SupportedAnalyticsService
  | SupportedMonitoringService
