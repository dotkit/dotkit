import { supported } from '@/store'
// TYPES
import type {
  SupportedService,
  SupportedCloudProvider,
  SupportedRegistryProvider,
  SupportedPackageManager,
  SupportedLanguage
} from './inputs'
import type { Command } from 'commander'
import type { TupleToUnionType } from './utils'

export type Dotkit = {
  program: Command
  workDir: string
  packageManager: SupportedPackageManager
  installedPackages: SupportedDotKitPackage[]
  config: {
    version: string
    target: 'container' | 'cluster'
    apps: {
      [key: string]: {
        image: string
        workDir: string
        language: SupportedLanguage
        specificity: { [key: string]: object | string }
        provider: {
          registry: SupportedRegistryProvider
          cloud: SupportedCloudProvider
        }
      }
    }
    services: {
      [key in SupportedService]: {
        provider: {
          cloud: SupportedCloudProvider
        }
      }
    }
  }
}

export type SupportedDotKitPackage =
  | SupportedService
  | SupportedCloudProvider
  | SupportedRegistryProvider

export type SupportedCommandOption = keyof typeof supported.options
export type SupportedInitScope = TupleToUnionType<typeof supported.options.init>
export type SupportedRunScope = TupleToUnionType<typeof supported.options.run>

/* export type AppSource = {
  workDir: string
  srcDir: string
  language: SupportedLanguage
  specificity: { [key: string]: object | string }
  provider: {
    registry: SupportedRegistryProvider
    cloud: SupportedCloudProvider
  }
}

export type AppImage = {
  image: string
  provider: {
    cloud: SupportedCloudProvider
  }
} */
