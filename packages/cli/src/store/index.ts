import * as fs from 'node:fs'
import * as path from 'node:path'
import YAML from 'yaml'
import { Command, Option } from 'commander'
import { Separator } from '@inquirer/prompts'
// TYPES
import type { Dotkit } from 'types/store'
import type {
  Choice,
  SupportedRegistryProvider,
  SupportedCloudProvider,
  SupportedService,
  SupportedPackageManager,
  SupportedLanguage
} from 'types/inputs'
import type { TupleToUnionType } from 'types/utils'

//==================== STATE ===================//

export const dotkit: Dotkit = {
  program: new Command()
    .version('0.0.1')
    .description('DOTkit is an Ops Toolkit')
    .configureOutput({
      outputError: (string, write) => write(`🧨 \x1b[31m${string}\x1b[0m`)
    })
    .showHelpAfterError()
    .addOption(
      new Option('-i, --init', 'create a configuration file').conflicts('run')
    )
    .addOption(
      new Option(
        '-r, --run',
        'run the pipelines according to configuration file'
      ).conflicts('init')
    )
    .parse(process.argv),
  workDir: '',
  packageManager: '' as SupportedPackageManager,
  installedPackages: [],
  config: {
    version: '0.0.1',
    target: 'container' as Dotkit['config']['target'],
    apps: {} as Dotkit['config']['apps'],
    services: {} as Dotkit['config']['services']
  }
}

// supported by dotkit cli
export const supported = {
  options: {
    init: ['app', 'service', 'infra'],
    run: ['build', 'deploy']
  },
  packageManager: ['npm', 'yarn', 'pnpm'],
  languages: ['javascript', 'python'],
  providers: {
    registry: ['aws', 'scaleway'],
    cloud: ['aws', 'scaleway']
  },
  services: {
    analytics: ['umami'],
    monitoring: ['grafana']
  }
} as const

// inputs
export const continueInputChoice: Choice<null> = {
  name: 'continue',
  value: null,
  description: 'Continue to next step'
}

export const returnInputChoice: Choice<null> = {
  name: 'return',
  value: null,
  description: 'Return to previous step'
}

//==================== ACTIONS ===================//

// getters
export const getDotkitConfig = async () => {
  const file = `${dotkit.workDir}/.kitrc`

  if (!fs.existsSync(file))
    return dotkit.program.error('No configuration file, please run dotkit -i')

  const content = fs.readFileSync(file, 'utf8')
  dotkit.config = { ...dotkit.config, ...YAML.parse(content) }

  console.log('Configuration file loaded')
}

export const getWorkingDirectory = async () => {
  const cwd = process.cwd()
  dotkit.workDir = path.join(cwd, '.')
}

export const getInputChoices = async <T>(
  supportedChoices: readonly T[]
): Promise<Choice<T>[]> =>
  supportedChoices.map((choice) => ({
    value: choice
  }))

export const getServicesInputChoices = async () => {
  const servicesInputChoices: Choice<SupportedService>[] = []
  for (const [name, services] of Object.entries(supported.services)) {
    servicesInputChoices.push(
      new Separator(`--- ${name.toUpperCase()} ---`),
      ...(await getInputChoices<TupleToUnionType<typeof services>>(services))
    )
  }
  return servicesInputChoices
}

// setters
export const setDotkitConfig = async () => {
  const { apps, services, ...rest } = dotkit.config

  const filteredDotKitConfig: Partial<Dotkit['config']> = { ...rest }

  // remove empty app or services field
  if (Object.keys(apps).length) filteredDotKitConfig.apps = apps
  if (Object.keys(services).length) filteredDotKitConfig.services = services

  const yaml = YAML.stringify(filteredDotKitConfig)

  fs.writeFile(`${dotkit.workDir}/.kitrc`, yaml, (error) => console.log(error))

  console.log(`Configuration file saved under ${dotkit.workDir}/.kitrc`)
}

// init

export const initDotkitConfig = async () => {
  await getWorkingDirectory()
  if (!dotkit.program.opts().init) await getDotkitConfig()
}

export const initAppConfig = async (
  appName: string,
  appType: 'source' | 'image'
) => {
  /*   if (appType === 'source') */
  dotkit.config.apps[appName] = {
    workDir: '',
    language: '' as SupportedLanguage,
    specificity: {},
    provider: {
      registry: '' as SupportedRegistryProvider,
      cloud: '' as SupportedCloudProvider
    },
    image: ''
  } /* as AppSource */
  /* else
    dotkit.config.apps[appName] = {
      image: '',
      provider: {
        cloud: '' as SupportedCloudProvider
      }
    } as AppImage */
}

export const initServiceConfig = async (serviceName: SupportedService) => {
  dotkit.config.services[serviceName] = {
    provider: { cloud: '' as SupportedCloudProvider }
  }
}
