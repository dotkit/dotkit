import { select, confirm } from '@inquirer/prompts'
// STORE
import { getInputChoices, supported } from '@/store'
// UTILS
import installPackage from '@/utils/installPackage'
// TYPES
import { SupportedRegistryProvider } from 'types/inputs'

export default async () => {
  const registryProvider = await select({
    message: 'Which registry provider do you want to use',
    choices: await getInputChoices<SupportedRegistryProvider>(
      supported.providers.registry
    )
  })

  await installPackage(registryProvider)

  return registryProvider
}
