import registryProviderSpecificInputs from './registry/index.js'
import cloudProviderSpecificInputs from './cloud/index.js'
import languageSpecificInputs from './languages/index.js'

export {
  registryProviderSpecificInputs,
  cloudProviderSpecificInputs,
  languageSpecificInputs
}
