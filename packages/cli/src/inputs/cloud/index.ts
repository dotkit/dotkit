import { select, confirm } from '@inquirer/prompts'
// STORE
import { getInputChoices, supported } from '@/store'
// UTILS
import installPackage from '@/utils/installPackage'
// TYPES
import { SupportedCloudProvider } from 'types/inputs'

export default async () => {
  const cloudProvider: 'aws' | 'scaleway' = await select({
    message: 'Which cloud provider do you want to use',
    choices: await getInputChoices<SupportedCloudProvider>(
      supported.providers.cloud
    )
  })

  await installPackage(cloudProvider)

  return cloudProvider
}
