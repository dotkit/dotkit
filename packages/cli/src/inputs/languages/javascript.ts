import * as fs from 'node:fs'
import { select } from '@inquirer/prompts'
// STORE
import { dotkit } from '@/store'
// UTILS
import { formatArrayToSelect } from '@/utils/inputs.js'
import { removeEmptyValueFromObject } from '@/utils/other.js'

export default async (appName: string) => {
  const appSourcePath = `${dotkit.config.apps[appName].workDir}`

  if (fs.existsSync(`${appSourcePath}/package.json`)) {
    // init package json
    const jsonPackage = JSON.parse(
      fs.readFileSync(`${appSourcePath}/package.json`, 'utf8')
    )
    // retrieve scripts without comments / empty values
    const jsonPackageScripts = removeEmptyValueFromObject(jsonPackage.scripts)

    //--- JS INPUTS ---//

    const buildCommand = await select({
      message: 'From your package.json, what is your build script command',
      choices: formatArrayToSelect(Object.keys(jsonPackageScripts))
    })

    return { buildCommand }
  }
  return dotkit.program.error(`No package.json found in ${appSourcePath}`)
}
