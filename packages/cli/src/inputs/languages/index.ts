// LANGUAGES
import javascript from './javascript'
import python from './python'
// TYPES
import type { SupportedLanguage } from 'types/inputs'

export default async (appName: string, language: SupportedLanguage) =>
  ({
    javascript: async () => await javascript(appName),
    python: async () => await python(appName)
  })[language]()
