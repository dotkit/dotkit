import Client, { Container } from '@dagger.io/dagger'
import 'dotenv/config'

//--- LOGGER ---//
export const logErrorAndExit = (string: string) => {
  console.log('\x1b[41m \x1b[37m=> ERROR: %s \x1b[0m', string)
  process.exit()
}

export const logInfo = (string: string) =>
  console.log('\x1b[46m \x1b[37m=> INFO: %s \x1b[0m', string)

export const logSuccess = (string: string) =>
  console.log('\x1b[42m \x1b[37m=> SUCCESS: %s \x1b[0m', string)

//--- VARIABLES CHECKER ----//
export const environmentVariablesChecker = (variables: string[]) => {
  for (const variable of variables) {
    if (!process.env[variable]) {
      logErrorAndExit(`${variable} variable must be set`)
    }
  }
}

//--- DAGGER UTILS ---//
export const setSecretEnvironmentVariables = (
  variables: { [key: string]: string },
  client: Client
) => {
  return (container: Container): Container => {
    Object.entries(variables).forEach(([key, value]) => {
      container = container.withSecretVariable(
        key,
        client.setSecret(key, value)
      )
    })
    return container
  }
}
