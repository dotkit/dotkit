// PROCESS
import { execSync } from 'child_process'
// INPUTS
import { confirm } from '@inquirer/prompts'
// STORE
import { dotkit } from '@/store'
// TYPES
import type { SupportedDotKitPackage } from 'types/store'

export default async (packageName: SupportedDotKitPackage) => {
  if (dotkit.installedPackages.includes(packageName)) return

  const canInstallPackage = await confirm({
    message:
      'I need to install the dotkit package dedicated to your cloud provider. Can I proceed?'
  })

  if (!canInstallPackage)
    return dotkit.program.error(
      'I need to install those packages in order to pursue this process.'
    )

  const packagePath = `@dotkit/${packageName}`

  console.log('Installing necessary packages, please wait...')

  const installCommand = {
    npm: `npm i -D ${packagePath}`,
    yarn: `yarn add ${packagePath} --dev`,
    pnpm: `pnpm add -D ${packagePath}`
  }[dotkit.packageManager]

  execSync(installCommand, { stdio: 'inherit' })

  dotkit.installedPackages.push(packageName)
}
