import fs from 'fs'
import path from 'path'

type FolderObject = {
  name: string
  path: string
  children?: { [key: string]: FolderObject }
}

const createFolderStructureObject = (
  folderPath: string,
  parentPath = ''
): FolderObject | null => {
  const stats = fs.statSync(folderPath)
  if (!stats.isDirectory()) {
    throw new Error(`${folderPath} is not a directory`)
  }

  const folderName = path.basename(folderPath)
  const fullPath = path.join(parentPath, folderName)
  const files = fs.readdirSync(folderPath)
  const gitignorePath = path.join(folderPath, '.gitignore')
  let ignoredFolders: string[] = []

  if (fs.existsSync(gitignorePath)) {
    const gitignoreContent = fs.readFileSync(gitignorePath, 'utf-8')
    ignoredFolders = gitignoreContent
      .split('\n')
      .map((line) => line.trim())
      .filter((line) => line && !line.startsWith('#'))
      .filter((line) => line.endsWith('/'))
      .map((line) => line.slice(0, -1))
  }

  const children: { [key: string]: FolderObject } = {}

  for (const file of files) {
    if (
      ignoredFolders.includes(file) ||
      file === '.git' ||
      file === '.vscode' ||
      file === folderName
    ) {
      continue
    }

    const filePath = path.join(folderPath, file)
    const fileStats = fs.statSync(filePath)
    if (fileStats.isDirectory()) {
      const childFolderObject = createFolderStructureObject(filePath, fullPath)
      if (childFolderObject) {
        children[childFolderObject.name] = childFolderObject
      }
    }
  }

  const folderObject: FolderObject = { name: folderName, path: fullPath }

  if (Object.keys(children).length > 0) {
    folderObject.children = children
  }

  return folderObject
}

const createPathUrlObject = (
  folderStructureObject: FolderObject
): { name: string; value: string }[] => {
  const pathUrlArray: { name: string; value: string }[] = []
  const queue: FolderObject[] = Object.values(
    folderStructureObject.children || {}
  )

  for (const folderObject of queue) {
    if (folderObject.path !== folderStructureObject.path) {
      const rootFolderName = folderStructureObject.name
      const relativePath = folderObject.path.replace(`${rootFolderName}/`, '')
      pathUrlArray.push({ name: folderObject.name, value: relativePath })
    }

    if (folderObject.children) {
      for (const childFolderObject of Object.values(folderObject.children)) {
        queue.push(childFolderObject)
      }
    }
  }

  return pathUrlArray
}

const tree = (folderPath: string) => {
  const folderStructureObject = createFolderStructureObject(folderPath)

  return folderStructureObject && createPathUrlObject(folderStructureObject)
}

export default tree
