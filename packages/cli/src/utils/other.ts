export const removeEmptyValueFromObject = (obj: { [key: string]: string }) => {
  return Object.entries(obj).reduce(
    (accumulator: { [key: string]: string }, [key, value]) => {
      if (value !== '') {
        accumulator[key] = value
      }
      return accumulator
    },
    {}
  )
}
