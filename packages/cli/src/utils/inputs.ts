export const formatArrayToSelect = <GenericType>(array: GenericType[]) => {
  return array.map((item) => ({ value: item }))
}
