#! /usr/bin/env node

/* import { DOTkitLogo } from './assets/DOTkit.js' */
// STORE
import { initDotkitConfig, dotkit } from './store'
// OPTIONS
import options from './options'
// TYPES
import type { SupportedCommandOption } from 'types/store'

//--- INIT ---//

//console.log(DOTkitLogo)

const commandOptions = dotkit.program.opts()

const start = async (selectedOption: SupportedCommandOption) =>
  ({
    init: async () => await options.init(),
    run: async () => await options.run()
  })[selectedOption]()

await initDotkitConfig()

// run selected options
for (const key of Object.keys(commandOptions) as SupportedCommandOption[])
  start(key)

console.log('The command has been executed with success')
