import { expect, test } from 'vitest'
// STORE
import { dotkit } from '@/store'
import { describe } from 'node:test'
/* export function makeProgram(options?: { exitOverride?: boolean, suppressOutput?: boolean }): Command {
  const program = new commander.Command();

  // Configuration
  if (options?.exitOverride) {
    program.exitOverride();
  }
  if (options?.suppressOutput) {
    program.configureOutput({
      writeOut: () => {},
      writeErr: () => {}
    });
  } */

describe('DOTkit CLI', async () => {
  test('should parse init option', async () => {
    process.argv = ['node', '../cli.ts', '-i']
    dotkit.program.parse(process.argv)
    expect(dotkit.program.opts().init).toBeTruthy()
  })

  test('should parse run option', async () => {
    process.argv = ['node', 'cli.ts', '-r']
    dotkit.program.parse(process.argv)
    expect(dotkit.program.opts().run).toBeTruthy()
  })
})
