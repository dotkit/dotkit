/* // INPUTS
import { confirm, select } from '@inquirer/prompts'
// STATE
import useStore from '@/store'
// ACTIONS
import build from '@/actions/build'
import deploy from '@/actions/deploy'
import infra from '@/actions/infra'
// OPTIONS
import next from '.'
// TYPES
import type {
  AvailableOption,
  AvailableInitAppAction,
  AvailableRunAppAction
} from 'types/inputs'

//--- INIT ---//

// store
const store = await useStore()

// app from source

const nextInitAppFromSourceAction =
  async (): Promise<AvailableInitAppAction | null> =>
    await select({
      message: 'Which action do you want to init for your app?',
      choices: [
        {
          name: 'create the registry infrastructure',
          value: 'infra:registry',
          description: 'provisioning the registry infrastructure via terraform'
        },
        {
          name: 'create the cloud infrastructure',
          value: 'infra:cloud',
          description: 'provisioning the cloud infrastructure via terraform'
        },
        store.returnInputChoice
      ]
    })

const nextRunAppFromSourceAction =
  async (): Promise<AvailableRunAppAction | null> =>
    await select({
      message: 'Which action do you want to run for your app?',
      choices: [
        {
          name: 'build my app',
          value: 'build',
          description:
            'create a containerized image of your app and host it on the registry'
        },
        {
          name: 'deploy my app',
          value: 'deploy',
          description:
            'deploy your containerized image to the defined cloud provider'
        },
        store.returnInputChoice
      ]
    })

// app from image

const nextInitAppFromImageAction =
  async (): Promise<AvailableInitAppAction | null> =>
    await select({
      message: 'Which action do you want to init for your app?',
      choices: [
        {
          name: 'create the cloud infrastructure',
          value: 'infra:cloud',
          description: 'provisioning the cloud infrastructure via terraform'
        },
        store.returnInputChoice
      ]
    })

const nextRunAppFromImageAction =
  async (): Promise<AvailableRunAppAction | null> =>
    await select({
      message: 'Which action do you want to run for your app?',
      choices: [
        {
          name: 'deploy my app',
          value: 'deploy',
          description:
            'deploy your containerized image to the defined cloud provider'
        },
        store.returnInputChoice
      ]
    })

const initAppAction = async (
  action: AvailableInitAppAction | AvailableRunAppAction
) =>
  ({
    'infra:registry': async () => await infra('registry'),
    'infra:cloud': async () => await infra('cloud'),
    build: async () => await build(),
    deploy: async () => await deploy()
  })[action]()

export default async (option: AvailableOption) => {
  let action: AvailableInitAppAction | AvailableRunAppAction | null

  if (store.dotKitAppOrigin === 'source')
    action =
      option === 'init'
        ? await nextInitAppFromSourceAction()
        : await nextRunAppFromSourceAction()
  else
    action =
      option === 'init'
        ? await nextInitAppFromImageAction()
        : await nextRunAppFromImageAction()

  if (!action) return next('init', 'index')

  return await initAppAction(action)
}
 */
