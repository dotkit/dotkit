// INIT
import init from '@/options/init'
import initApp from '@/options/init/app'
import initService from '@/options/init/app'
import initInfra from '@/options/init/infra'
// RUN
import run from '@/options/run'
import runBuild from '@/options/run/build'
import runDeploy from '@/options/run/deploy'
// STORE
import { supported } from '@/store'
// TYPES
import type {
  SupportedCommandOption,
  SupportedInitScope,
  SupportedRunScope
} from 'types/store'

/* const scope = ['app', 'service', 'infra'] as const

const isScope = (x: any): x is InitScope => scope.includes(x) */
export default async (
  option: SupportedCommandOption,
  scope: SupportedInitScope | SupportedRunScope
): Promise<void> => {
  // types guard
  const isInitOption = (
    scope: SupportedInitScope | SupportedRunScope
  ): scope is SupportedInitScope =>
    supported.options.init.includes(scope as SupportedInitScope)

  const isRunOption = (
    scope: SupportedInitScope | SupportedRunScope
  ): scope is SupportedRunScope =>
    supported.options.run.includes(scope as SupportedRunScope)

  if (isInitOption(scope) && option === 'init') await nextInit(scope)
  else if (isRunOption(scope) && option === 'run') await nextRun(scope)
}

const nextInit = async (scope: SupportedInitScope | 'index') =>
  ({
    index: async () => await init(),
    app: async () => await initApp(),
    service: async () => await initService(),
    infra: async () => await initInfra()
  })[scope]()

const nextRun = async (scope: SupportedRunScope | 'index') =>
  ({
    index: async () => await run(),
    build: async () => await runBuild(),
    deploy: async () => await runDeploy()
  })[scope]()
