// STATE
import {
  dotkit,
  getServicesInputChoices,
  continueInputChoice,
  initServiceConfig,
  setDotkitConfig
} from '@/store'
// SERVICE
import service from '.'
// INPUTS
import { select, confirm } from '@inquirer/prompts'
import { cloudProviderSpecificInputs } from '@/inputs'
// NEXT
import next from '@/options/next'
// TYPES
import type { SupportedService } from 'types/inputs'

export default async () => {
  //--- INIT ---//

  //--- SERVICE ---//
  const selectedService: SupportedService | null = await select({
    message: 'Which service do you want to add to your app?',
    choices: [...(await getServicesInputChoices()), continueInputChoice]
  })

  if (!selectedService) return

  await initServiceConfig(selectedService)

  //--- CLOUD PROVIDER SPECIFIC INPUTS ---//
  dotkit.config.services[selectedService].provider = {
    cloud: await cloudProviderSpecificInputs()
  }

  const addAnotherService: boolean = await confirm({
    message: 'Do you want to add another service?'
  })

  if (addAnotherService) await service()

  await setDotkitConfig()
  await next('init', 'infra')
}
