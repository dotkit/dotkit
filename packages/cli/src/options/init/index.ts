// INPUTS
import { select } from '@inquirer/prompts'
// STATE
import {
  dotkit,
  supported,
  getInputChoices,
  setDotkitConfig,
  getDotkitConfig
} from '@/store'
// SCOPE
import app from './app'
import service from './service'
import infra from './infra'
// NEXT
import next from '@/options/next'
// TYPES
import type { SupportedPackageManager } from 'types/inputs'
import type { SupportedInitScope } from 'types/store'

export default async () => {
  //--- INIT ---//

  // inputs
  dotkit.packageManager = await select({
    message: 'Which package manager are you using',
    choices: await getInputChoices<SupportedPackageManager>(
      supported.packageManager
    )
  })

  const chosenScope: SupportedInitScope | null = await select({
    message: 'What do you want to init',
    choices: [
      {
        value: 'app',
        name: 'my own application'
      },
      { value: 'service', name: 'a dotkit service' },
      {
        value: 'infra',
        name: 'the infrastructure of my application & services'
      },
      { value: null, name: 'exit' }
    ]
  })

  if (!chosenScope) return

  //--- FUNCTIONS ---//

  const init = async (scope: SupportedInitScope) =>
    ({
      app: async () => await app(),
      service: async () => await service(),
      infra: async () => {
        await getDotkitConfig()
        await infra()
      }
    })[scope]()

  await init(chosenScope)
}
