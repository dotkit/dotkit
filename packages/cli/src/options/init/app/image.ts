// STATE
import { dotkit } from '@/store'
// INPUTS
import { input } from '@inquirer/prompts'

export default async (appName: string) => {
  //--- INIT ---//

  dotkit.config.apps[appName].image = await input({
    message:
      "Enter the container image's full url (it must be publicly accessible)"
  })
}
