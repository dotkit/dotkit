// STATE
import { dotkit, supported, getInputChoices } from '@/store'
// INPUTS
import { select, confirm } from '@inquirer/prompts'
import {
  languageSpecificInputs,
  registryProviderSpecificInputs
} from '@/inputs/index.js'
import tree from '@/utils/tree.js'
import { SupportedLanguage } from 'types/inputs'

export default async (appName: string) => {
  //--- INIT ---//

  const appCurrentTree = tree(dotkit.workDir) || []
  if (!appCurrentTree.length)
    dotkit.program.error('Current directory has no folder structure')

  //--- USER GLOBAL INPUTS ---//

  dotkit.config.apps[appName].language = await select({
    message: 'In which language is your app written',
    choices: await getInputChoices<SupportedLanguage>(supported.languages)
  })

  dotkit.config.apps[appName].workDir = await select({
    message: `Which folder is your app ${appName} working directory`,
    choices: appCurrentTree
  })

  //--- LANGUAGE SPECIFIC INPUTS ---//

  dotkit.config.apps[appName].specificity = await languageSpecificInputs(
    appName,
    dotkit.config.apps[appName].language
  )

  //--- REGISTRY PROVIDER SPECIFIC INPUTS ---//
  dotkit.config.apps[appName].provider.registry =
    await registryProviderSpecificInputs()
}
