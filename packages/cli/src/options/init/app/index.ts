// STATE
import {
  dotkit,
  continueInputChoice,
  initAppConfig,
  setDotkitConfig
} from '@/store'
// INPUTS
import { select, input } from '@inquirer/prompts'
import { cloudProviderSpecificInputs } from '@/inputs'
// APP
import imageApp from './image'
import sourceApp from './source'
// NEXT
import next from '@/options/next'

export default async () => {
  //--- INIT ---//

  const appName = await input({ message: 'Enter your app name' })

  const appType: 'source' | 'image' | null = await select({
    message: 'Which origin will be your app build from?',
    choices: [
      {
        name: 'my own source code',
        value: 'source',
        description: 'work with the source of an application'
      },
      {
        name: 'a container image',
        value: 'image',
        description: 'work with an image of an application'
      },
      continueInputChoice
    ]
  })

  if (!appType) return

  await initAppConfig(appName, appType)

  appType === 'source' ? await sourceApp(appName) : await imageApp(appName)

  //--- CLOUD PROVIDER SPECIFIC INPUTS ---//
  dotkit.config.apps[appName].provider.cloud =
    await cloudProviderSpecificInputs()

  await setDotkitConfig()
  await next('init', 'infra')
}
