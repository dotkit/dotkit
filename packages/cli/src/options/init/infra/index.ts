// INPUTS
import { select } from '@inquirer/prompts'
// STORE
import { dotkit, getInputChoices } from '@/store'

export default async () => {
  const appKeys = Object.keys(dotkit.config.apps)
  if (!appKeys.length)
    dotkit.program.error(
      'No app in your config to provision an infrastructure for!'
    )

  const appName =
    appKeys.length === 1
      ? appKeys[0]
      : await select({
          message: 'For which app should we provision the infrastructure?',
          choices: await getInputChoices<string>(
            Object.keys(dotkit.config.apps)
          )
        })

  const infraScope: 'registry' | 'cloud' | null = await select({
    message: 'Which part of the infrastructure do you want to init?',
    choices: [
      {
        value: 'registry',
        name: 'the registry'
      },
      { value: 'cloud', name: 'the cloud' },
      { value: null, name: 'exit' }
    ]
  })
  if (!infraScope) return

  const provider = dotkit.config.apps[appName].provider[infraScope]
  const { default: infra } = await import(`@dotkit/${provider}`)

  const infraOperation:
    | 'plan'
    | 'apply'
    | 'destroy:plan'
    | 'destroy:apply'
    | null = await select({
    message: 'Which operation do you want to init?',
    choices: [
      {
        value: 'plan',
        name: 'plan'
      },
      { value: 'apply', name: 'apply' },
      { value: 'destroy:plan', name: 'plan destroy' },
      { value: 'destroy:apply', name: 'apply destroy' },
      { value: null, name: 'exit' }
    ]
  })
  if (!infraOperation) return

  const infraTarget: 'container' | 'cluster' | null = await select({
    message: 'On which target do you want to init?',
    choices: [
      {
        value: 'container',
        name: 'A serverless container'
      },
      { value: 'cluster', name: 'A managed kubernetes cluster' },
      { value: null, name: 'exit' }
    ]
  })
  if (!infraTarget) return

  await infra(
    infraTarget,
    infraOperation,
    `${dotkit.workDir}/artifacts/${appName}`
  )
}
