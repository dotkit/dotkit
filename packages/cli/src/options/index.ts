import init from './init/index.js'
import run from './run/index.js'

export default { init, run }
