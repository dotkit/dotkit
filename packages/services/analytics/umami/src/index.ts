import { connect, Client } from '@dagger.io/dagger'
import 'dotenv/config'

//--- DAGGER PIPELINES ---//

connect(
  async (client: Client) => {
    //--- INIT ---//
    // Dagger pipelines variables

    const configDirectory = client.host().directory('./src/config')

    const matomo = client.container().from('matomo:4.15.1')
  },
  { LogOutput: process.stderr }
)
