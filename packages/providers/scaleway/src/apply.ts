import * as fs from 'node:fs'
// TYPES
import type { Client, Directory, File } from '@dagger.io/dagger'

export default async (
  client: Client,
  terraformDirectory: Directory,
  workDir: string
) => {
  const plan = client.host().file(`${workDir}/infra/tfplan`)
  let state: File | null = null
  const stateFile = `${workDir}/infra/terraform.tfstate`

  if (!fs.existsSync(stateFile)) state = client.host().file(stateFile)

  const accessKey = client.setSecret(
    'scalewayAccessKey',
    process.env.SCALEWAY_ACCESS_KEY_ID || ''
  )
  const secretKey = client.setSecret(
    'scalewaySecretKey',
    process.env.SCALEWAY_SECRET_ACCESS_KEY || ''
  )
  const region = client.setSecret('region', process.env.SCALEWAY_REGION || '')
  const projectId = client.setSecret(
    'projectId',
    process.env.SCALEWAY_PROJECT_ID || ''
  )

  const terraform = client
    .container()
    .from('hashicorp/terraform:1.5.6')
    .withSecretVariable('SCW_ACCESS_KEY', accessKey)
    .withSecretVariable('SCW_SECRET_KEY', secretKey)
    .withSecretVariable('SCW_DEFAULT_REGION', region)
    .withSecretVariable('SCW_DEFAULT_PROJECT_ID', projectId)
    .withWorkdir('/usr/terraform')
    .withDirectory('.', terraformDirectory)
    .withFile('./tfplan', plan)

  if (state) terraform.withFile('./terraform.tfstate', state)

  await terraform
    .withExec(['init', '-input=false'])
    .withExec(['apply', '-input=false', 'tfplan'])
    .stdout()

  await terraform
    .file('/usr/terraform/tfplan')
    .export(`${workDir}/infra/tfplan`)
  await terraform
    .file('/usr/terraform/terraform.tfstate')
    .export(`${workDir}/infra/terraform.tfstate`)
}
