import type { Client, Directory } from '@dagger.io/dagger'

export const planDestroy = async (
  client: Client,
  terraformDirectory: Directory,
  workDir: string
) => {
  const plan = client.host().file('./infra/tfplan')
  const state = client.host().file('./infra/terraform.tfstate')

  const accessKey = client.setSecret(
    'scalewayAccessKey',
    process.env.SCALEWAY_ACCESS_KEY_ID || ''
  )
  const secretKey = client.setSecret(
    'scalewaySecretKey',
    process.env.SCALEWAY_SECRET_ACCESS_KEY || ''
  )
  const region = client.setSecret('region', process.env.SCALEWAY_REGION || '')
  const projectId = client.setSecret(
    'projectId',
    process.env.SCALEWAY_PROJECT_ID || ''
  )

  const terraform = client
    .container()
    .from('hashicorp/terraform:1.5.6')
    .withSecretVariable('SCW_ACCESS_KEY', accessKey)
    .withSecretVariable('SCW_SECRET_KEY', secretKey)
    .withSecretVariable('SCW_DEFAULT_REGION', region)
    .withSecretVariable('SCW_DEFAULT_PROJECT_ID', projectId)
    .withWorkdir('/usr/terraform')
    .withDirectory('.', terraformDirectory)
    .withFile('./tfplan', plan)
    .withFile('./terraform.tfstate', state)
    .withExec(['init', '-input=false'])
    .withExec([
      'plan',
      '-input=false',
      '-destroy',
      '-out=/usr/terraform/tfplan'
    ])

  await terraform.stdout()
  await terraform
    .file('/usr/terraform/tfplan')
    .export(`${workDir}/infra/tfplan`)
  await terraform
    .file('/usr/terraform/terraform.tfstate')
    .export(`${workDir}/terraform.tfstate`)
}

export const applyDestroy = async (
  client: Client,
  terraformDirectory: Directory,
  workDir: string
) => {
  const plan = client.host().file(`${workDir}/tfplan`)
  const state = client.host().file(`${workDir}/terraform.tfstate`)

  const accessKey = client.setSecret(
    'scalewayAccessKey',
    process.env.SCALEWAY_ACCESS_KEY_ID || ''
  )
  const secretKey = client.setSecret(
    'scalewaySecretKey',
    process.env.SCALEWAY_SECRET_ACCESS_KEY || ''
  )
  const region = client.setSecret('region', process.env.SCALEWAY_REGION || '')
  const projectId = client.setSecret(
    'projectId',
    process.env.SCALEWAY_PROJECT_ID || ''
  )

  return await client
    .container()
    .from('hashicorp/terraform:1.5.6')
    .withSecretVariable('SCW_ACCESS_KEY', accessKey)
    .withSecretVariable('SCW_SECRET_KEY', secretKey)
    .withSecretVariable('SCW_DEFAULT_REGION', region)
    .withSecretVariable('SCW_DEFAULT_PROJECT_ID', projectId)
    .withWorkdir('/usr/terraform')
    .withDirectory('.', terraformDirectory)
    .withFile('./tfplan', plan)
    .withFile('./terraform.tfstate', state)
    .withExec(['init', '-input=false'])
    .withExec(['apply', '-input=false', '-destroy', '-auto-approve'])
    .stdout()
}
