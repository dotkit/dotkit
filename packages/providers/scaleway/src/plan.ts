import type { Client, Directory } from '@dagger.io/dagger'

export default async (
  client: Client,
  terraformDirectory: Directory,
  workDir: string
) => {
  const terraform = client
    .container()
    .from('hashicorp/terraform:1.5.6')
    .withWorkdir('/usr/terraform')
    .withDirectory('.', terraformDirectory)
    .withExec(['init', '-input=false'])
    .withExec(['plan', '-input=false', '-out=/usr/terraform/tfplan'])

  await terraform.stdout()
  await terraform
    .file('/usr/terraform/tfplan')
    .export(`${workDir}/infra/tfplan`)
}
