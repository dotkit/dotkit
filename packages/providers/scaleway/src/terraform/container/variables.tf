//--- GLOBAL ---//

variable "tag" {
  type    = string
  default = "dotkit"
}

//--- CONTAINER ---//

variable "container_namespace" {
  type        = string
  description = "Serverless container namespace"
  default     = "dotkit-container"
}

variable "container_namespace_description" {
  type        = string
  description = "Serverless container namespace description"
  default     = "This namespace has been created by dotkit"
}




