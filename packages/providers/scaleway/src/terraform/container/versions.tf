terraform {
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "= 2.27.0"
    }
  }
  required_version = "= 1.5.6"
}
