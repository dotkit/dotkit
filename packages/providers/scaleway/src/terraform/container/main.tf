provider "scaleway" {}

resource "scaleway_container_namespace" "main" {
  name        = var.container_namespace
  description = var.container_namespace_description
}

resource "scaleway_container" "main" {
  name            = "my-container-02"
  description     = "environment variables test"
  namespace_id    = scaleway_container_namespace.main.id
  registry_image  = "registry.hub.docker.com/library/alpine:latest"
  port            = 9997
  cpu_limit       = 1000
  memory_limit    = 256
  min_scale       = 0
  max_scale       = 2
  timeout         = 600
  max_concurrency = 80
  privacy         = "private"
  protocol        = "h2c"
  deploy          = true

  environment_variables = {
    "foo" = "var"
  }
  secret_environment_variables = {
    "key" = "secret"
  }
}
