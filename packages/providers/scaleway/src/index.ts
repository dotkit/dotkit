import { connect, Client } from '@dagger.io/dagger'
import 'dotenv/config'
import plan from './plan.js'
import apply from './apply.js'
import { planDestroy, applyDestroy } from './destroy.js'

//--- DAGGER PIPELINES ---//

export default (
  target: 'container' | 'cluster',
  operation: 'plan' | 'apply' | 'destroy:plan' | 'destroy:apply',
  workDir: string
) =>
  connect(
    async (client: Client) => {
      //--- INIT ---//
      // Dagger pipelines variables
      const terraformDirectory = client
        .host()
        .directory(
          `${process.cwd()}/node_modules/@dotkit/scaleway/dist/terraform/${target}`
        )

      if (operation === 'plan') await plan(client, terraformDirectory, workDir)
      if (operation === 'apply')
        await apply(client, terraformDirectory, workDir)
      if (operation === 'destroy:plan')
        await planDestroy(client, terraformDirectory, workDir)
      if (operation === 'destroy:apply')
        await applyDestroy(client, terraformDirectory, workDir)
    },
    { LogOutput: process.stderr }
  )
