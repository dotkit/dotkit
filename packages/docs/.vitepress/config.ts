/// import "vitepress/client"
import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'DOTkit',
  description: 'DOTkit is an Ops Toolkit',
  srcDir: 'src',
  outDir: '../../public',
  cleanUrls: true,
  markdown: {
    theme: {
      light: 'one-dark-pro',
      dark: 'one-dark-pro'
    },
    lineNumbers: true
  },
  head: [
    [
      'link',
      {
        rel: 'icon',
        href: '/dotkit/faviconDark.ico',
        media: '(prefers-color-scheme: light)'
      }
    ],
    [
      'link',
      {
        rel: 'icon',
        href: '/dotkit/faviconWhite.ico',
        media: '(prefers-color-scheme: dark)'
      }
    ]
  ],
  themeConfig: {
    logo: {
      light: '/DOTkit_light.png',
      dark: '/DOTkit_dark.png',
      alt: 'DOTkit'
    },
    // https://vitepress.dev/reference/default-theme-config
    nav: [{ text: 'Guide', link: '/guide/getting-started' }],
    search: {
      provider: 'local'
    },
    sidebar: {
      '/guide/': [
        {
          text: 'Introduction',
          collapsed: false,
          items: [
            { text: 'What is DOTkit?', link: '/guide/what-is-dotkit' }
            /*  { text: 'Getting Started', link: '/guide/getting-started' } */
          ]
        }
        /* {
          text: 'Core',
          collapsed: false,
          items: [{ text: 'Index', link: '/core/index' }]
        },
        {
          text: 'Providers',
          collapsed: false,
          items: [{ text: 'Index', link: '/providers/index' }]
        },
        {
          text: 'Services',
          collapsed: false,
          items: [{ text: 'Index', link: '/services/index' }]
        } */
      ]
    },

    socialLinks: [
      {
        icon: {
          svg: '<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 23.113"><title>Gitlab</title><path d="m23.601 9.15-.033-.087L20.3.538a.851.851 0 0 0-.336-.405.875.875 0 0 0-1 .054.875.875 0 0 0-.29.44L16.47 7.374H7.538L5.332.626a.857.857 0 0 0-.29-.44.875.875 0 0 0-1-.054.859.859 0 0 0-.336.404L.433 9.058l-.032.086a6.066 6.066 0 0 0 2.012 7.011l.011.009.03.021 4.976 3.727 2.462 1.863 1.5 1.132a1.009 1.009 0 0 0 1.22 0l1.5-1.132 2.461-1.863 5.006-3.75.013-.01a6.069 6.069 0 0 0 2.01-7.003z"/></svg>'
        },
        link: 'https://gitlab.com/dotkit/dotkit'
      }
    ],
    footer: {
      message: 'Released under the MIT License',
      copyright: 'Copyright © 2023 <-> present => Michel Guerin'
    }
  }
})
