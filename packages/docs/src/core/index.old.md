# Core
```mermaid
flowchart TD
    A[Start] --> B{Is it?}
    B -->|Yes| C[OK]
    C --> D[Rethink]
    D --> B
    B ---->|No| E[End]
```

```mermaid
flowchart TB
    c1-->a2
    subgraph one
    a1-->a2
    end
    subgraph two
    b1-->b2
    end
    subgraph three
    c1-->c2
    end
```
## Init

```mermaid
flowchart TD;
    I0(init)-->I1.1(app);
    I0(init)-->I1.2(service);
    click I0 "#app"
    click I0 "#service"
```

## App

```mermaid
flowchart TD;
    A0(app)-->A1.1(language);
```

## Service

```mermaid
flowchart TD;
    S0(service)-->S1.1(language);
```
