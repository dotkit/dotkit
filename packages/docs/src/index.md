---
layout: home

hero:
  name: DOTkit
  text: is an Ops Toolkit
  tagline: Pipelines as a Service - From local to production in minutes  
  image:
    light: /DOTkit_light.png
    dark: /DOTkit_dark.png
    alt: DOTkit
  actions:
    - theme: brand
      text: Get Started
      link: /guide/what-is-dotkit
    - theme: alt
      text: View on Gitlab
      link: https://gitlab.com/dotkit/dotkit

features:
 - icon: 🚀
   title: Pipelines as a Service
   details: Choose on which cloud provider to build and deploy your app
 - icon: 📝
   title: Declarative pipelines
   details: A CLI made to help you describe your desired outcome, step by step
 - icon: 🪡
   title: Tailored services to choose from
   details: Add preconfigured services to your infrastructure that will fit your app
---