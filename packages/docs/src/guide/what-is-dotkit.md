# What is DOTkit

DotKIT is in pre-alpha. The aim of this project is to support small to mid-sized team in their journey towards devops culture. It might help them to build and deploy their app in no time, based on predefined IaC scripts backed inside dagger pipelines.

A long way is ahead, so stay tuned for the next developments!

Documentation is in progress.

This diagram is a raw vision of what dotKIT tries to achieve. 

![DOTkit vision](./vision.drawio.svg)